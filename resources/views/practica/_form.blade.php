<div>
    <label>Titulo</label>
    <input type="text" name="titulo" required value="{{ old('titulo', $practica->titulo) }}">
    @error('titulo')
        <div>{{ $message }}</div>
    @enderror
</div>
<div>
    <label>Fichero</label>
    <div>{{ $practica->fichero }}</div>
    <input type="file" name="fichero" value="{{ $practica->fichero }}">
    @error('fichero')
        <div>{{ $message }}</div>
    @enderror
</div>
<div>
    <label>Curso</label>
    <select name="curso_id">
        <option value="">-- Seleccione --</option>
        @foreach ($cursos as $curso)
            <option value="{{ $curso->id }}"
                {{ old('curso_id', $practica->curso_id) == $curso->id ? 'selected' : '' }}>
                {{ $curso->nombre }}</option>
        @endforeach
    </select>
    @error('curso_id')
        <div>{{ $message }}</div>
    @enderror
</div>
<div>
    <button type="submit" class="boton">Enviar</button>
</div>
