<?php

namespace App\Http\Controllers;

use App\Http\Requests\PracticaStoreRequest;
use App\Http\Requests\PracticaUpdateRequest;
use App\Models\Curso;
use App\Models\Practica;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rules\File;

class PracticaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $practicas = Practica::all();

        return view(
            'practica.index',
            compact('practicas')
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // esto es para mostrar un dropdown en curso_id
        $cursos = Curso::all();
        $practica = new Practica();

        return view(
            'practica.create',
            compact('cursos', 'practica')
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PracticaStoreRequest $request)
    {

        $ficheroEnviado = $request->file('fichero');
        $fichero = $ficheroEnviado->store('ficheros', 'public');
        $practica = new Practica();
        $practica->fill($request->all());
        $practica->fichero = $fichero;
        $practica->save();


        return redirect()
            ->route('practica.show', $practica);
    }

    /**
     * Display the specified resource.
     */
    public function show(Practica $practica)
    {
        return view(
            'practica.show',
            compact('practica')
        );
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Practica $practica)
    {
        // esto es para mostrar un dropdown en curso_id
        $cursos = Curso::all();

        return view(
            'practica.edit',
            compact('practica', 'cursos')
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PracticaUpdateRequest $request, Practica $practica)
    {
        // comprobar si ha subido un nuevo archivo
        if ($request->hasFile('fichero')) {

            // archivo nuevo
            // $request->file('fichero') // objeto

            // archivo antiguo
            // $practica->fichero // string 

            // recupero el archivo
            $ficheroEnviado = $request->file('fichero');

            // elimino el archivo anterior
            Storage::disk('public')->delete($practica->fichero);

            // guardo el nuevo archivo
            $fichero = $ficheroEnviado->store('ficheros', 'public');

            // actualizo el registro
            $practica->fill($request->all());

            // sustituir el nombre del archivo antiguo por el nombre
            // del archivo subido
            $practica->fichero = $fichero;
        } else {
            // si no subo un nuevo archivo
            $practica->fill($request->all());
        }

        $practica->save();

        return redirect()
            ->route('practica.show', $practica);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Practica $practica)
    {
        // elimino el archivo
        Storage::disk('public')->delete($practica->fichero);

        // elimino el registro
        $practica->delete();

        return redirect()
            ->route('practica.index');
    }

    public function confirmar(Practica $practica)
    {
        return view('practica.confirmar', compact('practica'));
    }
}
