        <div>
            <label>Nombre</label>
            <input type="text" name="nombre" value="{{ old('nombre', $alumno->nombre) }}">
            @error('nombre')
                <div>{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label>Apellidos</label>
            <input type="text" name="apellidos" value="{{ old('apellidos', $alumno->apellidos) }}">
            @error('apellidos')
                <div>{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label>Email</label>
            <input type="email" name="email" value="{{ old('email', $alumno->email) }}">
            @error('email')
                <div>{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label>Fecha Nacimiento</label>
            <input type="date" name="fechanacimiento" value="{{ old('fechanacimiento', $alumno->fechanacimiento) }}">
            @error('fechanacimiento')
                <div>{{ $message }}</div>
            @enderror
        </div>
        <div>
            <label>Foto</label>
            @if ($alumno->foto)
                <img src="{{ asset('storage/' . $alumno->foto) }}" id="preview">
            @else
                <img id="preview">
            @endif
            <input type="file" name="foto" value="{{ $alumno->foto }}" id="fichero">
            @error('foto')
                <div>{{ $message }}</div>
            @enderror
        </div>
        <div>
            <button type="submit">Enviar</button>
        </div>
