@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <form action="{{ route('practica.update', $practica) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('put')
            @include('practica._form')
        </form>
    </div>
@endSection
