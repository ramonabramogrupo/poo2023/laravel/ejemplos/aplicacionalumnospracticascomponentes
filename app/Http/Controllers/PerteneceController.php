<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use App\Models\Curso;
use App\Models\Pertenece;
use Illuminate\Http\Request;

class PerteneceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $perteneces = Pertenece::all();

        return view(
            'pertenece.index',
            compact('perteneces')
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // esto es para el dropdown
        $alumnos = Alumno::all();
        $cursos = Curso::all();

        return view('pertenece.create', compact('alumnos', 'cursos'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $pertenece = Pertenece::create($request->all());

        return redirect()
            ->route('pertenece.show', $pertenece);
    }

    /**
     * Display the specified resource.
     */
    public function show(Pertenece $pertenece)
    {
        return view(
            'pertenece.show',
            compact('pertenece')
        );
    }

    /**r
     * Show the form for editing the specified resource.
     */
    public function edit(Pertenece $pertenece)
    {
        // esto es para el dropdown
        $alumnos = Alumno::all();
        $cursos = Curso::all();

        return view(
            'pertenece.edit',
            compact('pertenece', 'alumnos', 'cursos')
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Pertenece $pertenece)
    {
        $pertenece->update($request->all());

        return redirect()
            ->route('pertenece.show', $pertenece);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Pertenece $pertenece)
    {
        $pertenece->delete();

        return redirect()
            ->route('pertenece.index');
    }

    public function confirmar(Pertenece $pertenece)
    {
        return view('pertenece.confirmar', compact('pertenece'));
    }
}
