@extends('layouts.main')

@section('content')
    <h1>Eliminando registro</h1>
    <div>
        ¿Estas seguro de que quieres borrar el registro?
    </div>
    <div class="tarjeta">
        <ul>
            <li>id:{{ $pertenece->id }}</li>
            <li>Alumno:{{ $pertenece->alumno_id }} - {{ $pertenece->alumno->nombre }}</li>
            <li>Curso:{{ $pertenece->curso_id }} - {{ $pertenece->curso->nombre }}</li>
        </ul>
        <form action="{{ route('pertenece.destroy', $pertenece) }}" method="post" class="form-inline">
            @csrf
            @method('delete')
            <button type="submit" class="boton">Borrar</button>
        </form>
    </div>
@endsection
