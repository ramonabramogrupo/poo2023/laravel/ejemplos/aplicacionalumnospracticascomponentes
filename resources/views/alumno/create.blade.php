@extends('layouts.main')

@section('content')
    <form action="{{ route('alumno.store') }}" method="post" enctype="multipart/form-data">
        @csrf
        @include('alumno._form')

    </form>
@endsection
@section('css')
    <style>
        #preview {
            max-width: 100%;
            width: 200px;
        }
    </style>
@endsection
