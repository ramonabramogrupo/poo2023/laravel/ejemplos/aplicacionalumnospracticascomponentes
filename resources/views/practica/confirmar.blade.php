@extends('layouts.main')

@section('content')
    <h1>Eliminando registro</h1>
    <div>
        ¿Estas seguro de que quieres borrar el registro?
    </div>
    <div class="tarjeta">
        <ul>
            <li>id:{{ $practica->id }}</li>
            <li>Titulo:{{ $practica->titulo }}</li>
            <li>Fichero:{{ $practica->fichero }}</li>
            <li>Curso:{{ $practica->curso_id }} - {{ $practica->curso->nombre }}</li>
        </ul>
        <form action="{{ route('practica.destroy', $practica) }}" method="post" class="form-inline">
            @csrf
            @method('delete')
            <button type="submit" class="boton">Borrar</button>
        </form>
    </div>
@endsection
