@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <form action="{{ route('practica.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('practica._form')
        </form>
    </div>
@endSection
