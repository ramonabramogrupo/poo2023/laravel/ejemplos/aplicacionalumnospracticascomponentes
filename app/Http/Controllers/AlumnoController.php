<?php

namespace App\Http\Controllers;

use App\Http\Requests\AlumnoRequest;
use App\Http\Requests\AlumnoStoreRequest;
use App\Http\Requests\AlumnoUpdateRequest;
use App\Models\Alumno;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\File;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // obtener todos los alumnos
        $alumnos = Alumno::all(); // coleccion (objeto de tipo coleccion) de objetos (tipo Alumno)

        // retornar vista con datos
        return view(
            'alumno.index',
            compact('alumnos')
        );
    }

    /**
     * Esta accion solo es para cargar el formulario
     */
    public function create()
    {
        return view('alumno.create', [
            'alumno' => new Alumno()
        ]);
    }

    /**
     * Esta accion es para crear un nuevo alumno cuando pulse el boton añadir
     */
    public function store(AlumnoStoreRequest $request)
    {

        // obtengo la foto subida
        $foto = $request->file('foto');

        // la almaceno en el disco publico
        $fichero = $foto->store('fotos', 'public');

        // creo el elemento en la bbdd
        $alumno = new Alumno();
        $alumno->fill($request->all());
        $alumno->foto = $fichero;
        $alumno->save();

        // redirecciono a la accion show para mostrar el registro creado
        return redirect()
            ->route('alumno.show', $alumno);
    }

    /**
     * Display the specified resource.
     */
    public function show(Alumno $alumno)
    {
        return view('alumno.show', compact('alumno'));
    }

    /**
     * Mostrar formulario para editar un alumno  y le llega el alumno a editar
     */
    public function edit(Alumno $alumno)
    {
        // mandar los datos al formulario para que los pueda editar el usuario
        return view('alumno.edit', compact('alumno'));
    }

    /**
     * recibe un alumno 
     * request (datos escritos en el formulario)
     * alumno (datos del alumno almacenados en la bbdd)
     */
    public function update(AlumnoUpdateRequest $request, Alumno $alumno)
    {

        if ($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $fichero = $foto->store('fotos', 'public');
            Storage::disk('public')->delete($alumno->foto);
            $alumno->fill($request->all());
            $alumno->foto = $fichero;
        } else {
            $alumno->fill($request->all());
        }

        $alumno->save();

        return redirect()
            ->route('alumno.show', $alumno);
    }

    /**
     * Me llega un alumno y debo eliminarle
     */
    public function destroy(Alumno $alumno)
    {
        // eliminamos la foto
        Storage::disk('public')->delete($alumno->foto);

        // eliminamos el registro
        $alumno->delete();

        return redirect()
            ->route('alumno.index');
    }
}
