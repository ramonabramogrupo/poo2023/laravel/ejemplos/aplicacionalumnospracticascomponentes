@extends('layouts.main')

@section('content')
    <div class="tarjeta">
        <ul>
            <li>id:{{ $pertenece->id }}</li>
            <li>Titulo:{{ $pertenece->alumno_id }} - {{ $pertenece->alumno->nombre }}</li>
            <li>Curso:{{ $pertenece->curso_id }} - {{ $pertenece->curso->nombre }}</li>
        </ul>
        <div class="botones">
            <a href="{{ route('pertenece.edit', $pertenece) }}" class="boton">Actualizar</a>
            <form action="{{ route('pertenece.destroy', $pertenece) }}" method="post" id="eliminar" class="form-inline">
                @csrf
                @method('delete')
                <button type="submit" class="boton">Borrar</button>
            </form>
            <a href="{{ route('pertenece.confirmar', $pertenece) }}" class="boton">Eliminar otra forma</a>
        </div>
    </div>
@endsection
