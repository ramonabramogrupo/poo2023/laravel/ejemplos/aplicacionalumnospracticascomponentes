@extends('layouts.main')

@section('content')
    <ul>
        <li>id:{{ $alumno->id }}</li>
        <li>Nombre:{{ $alumno->nombre }}</li>
        <li>Apellidos:{{ $alumno->apellidos }}</li>
        <li>Email:{{ $alumno->email }}</li>
        <li>fecha Nacimiento:{{ $alumno->fechanacimiento }}</li>
        <li>Foto: <br>
            <img class="imagen" src="{{ asset('storage/' . $alumno->foto) }}">
        </li>
        <li>
            <a href="{{ route('alumno.edit', $alumno) }}">Actualizar</a>
            <form action="{{ route('alumno.destroy', $alumno) }}" method="post" id="eliminar">
                @csrf
                @method('delete')
                <button type="submit">Borrar</button>
            </form>
        </li>
    </ul>
@endsection

@section('css')
    <style>
        .imagen {
            max-width: 100%;
            width: 200px;
        }
    </style>
@endsection
