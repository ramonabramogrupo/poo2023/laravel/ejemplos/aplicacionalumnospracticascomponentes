<?php

namespace Database\Seeders;

use App\Models\Curso;
use App\Models\Practica;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PracticaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // numero de cursos a crear
        for ($numeroCurso = 0; $numeroCurso < 10; $numeroCurso++) {
            $curso = Curso::factory()->create();

            // numero de practicas por curso
            $totalPracticas = rand(1, 10); // numero entero aleatorio entre 1 y 10
            for ($numeroPracticas = 0; $numeroPracticas < $totalPracticas; $numeroPracticas++)
                Practica::factory()
                    ->for($curso)
                    ->create();
        }
    }
}
